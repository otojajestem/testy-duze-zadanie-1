#include "test.h"
#include "example.h"
#include "cannot_change_arguments.h"
#include "stress.h"
#include "corner_cases.h"

void Testuj() {
	RUN_TEST(Example);
	RUN_TEST(CannotChangeArguments);
	RUN_TEST(CornerCases);
	RUN_TEST(StressCorrect);
	RUN_TEST(StressPerformance);
}
